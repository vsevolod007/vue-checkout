import Vue from 'vue';
import Vuex from 'vuex';

import contactDetails from "./modules/ContactDetails"
import count from "./modules/Count"
import deliveryMethods from "./modules/DeliveryPrice"

Vue.use(Vuex);


export const store = new Vuex.Store({
    strict: process.env.NODE_ENV !== 'production',
    state: {
        picked: null,
        deliveryCost: null,
        price: 1000
    },
    getters: {
        picked: (state) => state.picked,
        totalPrice: (state) => Number(state.price ) + Number(state.deliveryCost),
        deliveryCost: (state) => state.deliveryCost,

    },
    mutations: {
        changeCost (state, value) {
            state.deliveryCost = value
        },
        changePicked (state, value) {
            state.picked = value,
            state.deliveryCost = null
        }
    },
    actions: {
        changeCost: (store, value) => store.commit("changeCost", value),
        changePicked: (store, value) => store.commit("changePicked", value)
    },
    modules: {
        contactDetails,
        count,
        deliveryMethods
    }
})