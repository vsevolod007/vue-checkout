import axios from "axios"
export default {
    namespaced: true,
    state: {
        // formGroup: [
        //     {
        //         "label" :"Имя",
        //         "value" :"Всеволод",
        //         "nameInput" :"customers_firstname",
        //         "requiredShow" : true,
        //         "errorMessage" : "Укажите имя"
        //     },
        //     {
        //         "label" :"Фамилия",
        //         "value" :"Стародубов",
        //         "nameInput" :"customers_lastname",
        //         "requiredShow" : true,
        //         "errorMessage" : "Укажите фамилию"
        //     },
        //     {
        //         "label" :"Телефон",
        //         "type" : "phone",
        //         "value" :"+384646846",
        //         "nameInput" :"customers_telephone",
        //         "requiredShow" : true,
        //         "phoneMasks" : /^[0-9]{7,14}$/,
        //         "errorMessage" : "Введите существующий номер телефона с кодом города. Например +7(495)123-45-67"
        //     },
        //     {
        //         "label" :"Город",
        //         "value" :"г. Москва",
        //         "nameInput" :"entry_city",
        //         "requiredShow" : true,
        //         "errorMessage" : "Город указан некорректно. Проверьте правильность написания города или укажите ближайший город, если вы живете в населенном пункте, после чего в комментарии к заказу уточните Ваш адрес"
        //     }
        //
        // ],
        formGroup: []
    },
    getters: {
        formGroup: state => {
            return state.formGroup
        },
        // formGroup2: state => {
        //     return state.formGroup2
        // }
    },
    mutations: {
        getList(state, payload) {
            state.formGroup = payload
        },
        setList(state, payload) {
            let [index, value] = payload;
            state.formGroup[index].value = value
            // console.log(state.formGroup[index])
        }
    },
    actions: {
        getListApi(store) {
            axios
                .get('http://www.mocky.io/v2/5daeb85a320000ed71d95c1b')
                .then(response => (store.commit('getList', response.data)))
                .catch(error => {
                    console.log(error)
                })
        },
        setList(store, payload) {
            store.commit('setList', payload)
        }
    }
};