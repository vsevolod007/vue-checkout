export default {
    namespaced: true,
    state: {
        deliveryMethods: [
            {'name': 'Офис мамси ', "cost": 0},
            {'name': 'Курьером ', "cost": 376},
            {'name': ' Почтой ', "cost": 174},
        ],
        deliveryMethodsExtra: [
            {'name': ' Такси ', "cost": 1094},
        ],

    },
    getters: {
        deliveryMethods: (state) => state.deliveryMethods,
        deliveryMethods2: (state) => [...state.deliveryMethods, ...state.deliveryMethodsExtra]
    },
    mutations: {
    },
    actions: {
    }
}