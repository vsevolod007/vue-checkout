export default {
    namespaced: true,
    state: {
        count: 0
    },
    getters: {
        count: (state) => state.count
    },
    mutations: {
        increment (state) {
            state.count++
        }
    },
    actions: {
        increment: (store) => store.commit("increment")
    }
}